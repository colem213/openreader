use crate::database::UuidProxy;
use crate::database::schema::itunes_feed_extensions;

use rss::extension::itunes::ITunesChannelExtension;

#[derive(Queryable, Selectable, Insertable, AsChangeset)]
#[diesel(table_name = itunes_feed_extensions)]
pub struct ItunesFeedExtension {
  pub id: UuidProxy,
  pub subtitle: Option<Option<String>>,
  pub kind: Option<Option<String>>,
  pub author: Option<Option<String>>,
  pub owner_name: Option<Option<String>>,
  pub owner_email: Option<Option<String>>,
  pub summary: Option<Option<String>>,
  pub explicit: Option<Option<bool>>,
  pub complete: Option<bool>,
  pub block: Option<bool>,
  pub image: Option<Option<String>>,
  pub new_feed_url: Option<Option<String>>,
}

impl ItunesFeedExtension {
  pub fn new(id: UuidProxy, value: ITunesChannelExtension) -> Self {
    Self {
      id,
      subtitle: Some(value.subtitle),
      kind: Some(
        value.r#type.as_ref().and_then(|v|
          Some(v.clone().to_lowercase())
        )
      ),
      author: Some(value.author),
      owner_name: Some(
        value.owner.as_ref().and_then(|v|
          v.name.clone()
        )
      ),
      owner_email: Some(
        value.owner.as_ref().and_then(|v|
          v.email.clone()
        )
      ),
      summary: Some(value.summary),
      explicit: Some(
        value.explicit.and_then(|v|
          match v.to_lowercase().as_str() {
            "yes" | "explicit" | "true" => Some(true),
            "no" | "clean" | "false" => Some(false),
            _ => None,
          }
        )
      ),
      complete: value.complete.and_then(|v|
        match v.to_lowercase().as_str() {
          "yes" | "true" => Some(true),
          _ => None,
        }
      ),
      block: value.block.and_then(|v|
        match v.to_lowercase().as_str() {
          "yes" | "true" => Some(true),
          _ => None,
        }
      ),
      image: Some(value.image),
      new_feed_url: Some(value.new_feed_url),
    }
  }
}
