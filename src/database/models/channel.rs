use crate::atom::AtomExtension;
use crate::database::multi_connection_impl::MultiBackend;
use crate::database::UuidProxy;
use crate::database::schema::channels;
use crate::podcast::PodcastExtension;

use diesel::sqlite::Sqlite;
use rss::Channel as RssChannel;
use time::{OffsetDateTime, format_description::well_known::Rfc2822};
use ulid::Ulid;

#[derive(Queryable, Selectable)]
#[diesel(table_name = channels, check_for_backend(MultiBackend, Sqlite))]
pub struct Channel {
  pub id: UuidProxy,
  pub title: String,
  pub link: String,
  pub description: String,
  pub language: Option<String>,
  pub copyright: Option<String>,
  pub managing_editor: Option<String>,
  pub webmaster: Option<String>,
  pub published_at: Option<OffsetDateTime>,
  pub last_build_at: Option<OffsetDateTime>,
  pub generator: Option<String>,
  pub docs: Option<String>,
  pub rating: Option<String>,
  pub ttl: Option<i32>,
  pub image: Option<String>,
  pub created_at: OffsetDateTime,
  pub updated_at: OffsetDateTime,
}

impl Channel {
  pub fn from_rss_channel_guid(channel: &RssChannel) -> String {
    let atom = AtomExtension::try_from(&channel.extensions).ok();
    let podcast = PodcastExtension::try_from(&channel.extensions).ok();
    atom.as_ref().and_then(|v| v.id.clone())
      .or(podcast.as_ref().and_then(|v| v.guid.clone()))
      .unwrap_or(String::from(""))
  }

  pub fn from_rss_channel(channel: &RssChannel) -> Self {
    Channel {
      id: Ulid::new().into(),
      title: channel.title.clone(),
      link: channel.link.clone(),
      description: channel.description.clone(),
      language: channel.language.clone(),
      copyright: channel.copyright.clone(),
      managing_editor: channel.managing_editor.clone(),
      webmaster: channel.webmaster.clone(),
      published_at: channel.pub_date.as_ref().and_then(|v|
        OffsetDateTime::parse(v.as_str(), &Rfc2822).ok()
      ),
      last_build_at: channel.last_build_date.as_ref().and_then(|v|
        OffsetDateTime::parse(v.as_str(), &Rfc2822).ok()
      ),
      generator: channel.generator.clone(),
      docs: channel.docs.clone(),
      rating: channel.rating.clone(),
      ttl: channel.ttl.as_ref().and_then(|v|
        v.parse::<i32>().ok().and_then(|v| Some(v))
      ),
      image: None,
      created_at: OffsetDateTime::now_utc(),
      updated_at: OffsetDateTime::now_utc(),
    }
  }
}
