use crate::database::schema::skip_hours;
use crate::error::Error;

use diesel::{Queryable, Selectable};
use diesel::backend::Backend;
use diesel::deserialize::{self, FromSql};
use diesel::sqlite::Sqlite;
use diesel::sql_types::Integer;

#[derive(Debug, Clone, Copy)]
pub enum SkipHours {
  Hour0 = 0,
  Hour1 = 1,
  Hour2 = 2,
  Hour3 = 3,
  Hour4 = 4,
  Hour5 = 5,
  Hour6 = 6,
  Hour7 = 7,
  Hour8 = 8,
  Hour9 = 9,
  Hour10 = 10,
  Hour11 = 11,
  Hour12 = 12,
  Hour13 = 13,
  Hour14 = 14,
  Hour15 = 15,
  Hour16 = 16,
  Hour17 = 17,
  Hour18 = 18,
  Hour19 = 19,
  Hour20 = 20,
  Hour21 = 21,
  Hour22 = 22,
  Hour23 = 23,
}

impl TryFrom<i32> for SkipHours {
  type Error = Box<dyn std::error::Error + Send + Sync>;

  fn try_from(value: i32) -> Result<Self, Self::Error> {
    match value {
      0 => Ok(SkipHours::Hour0),
      1 => Ok(SkipHours::Hour1),
      2 => Ok(SkipHours::Hour2),
      3 => Ok(SkipHours::Hour3),
      4 => Ok(SkipHours::Hour4),
      5 => Ok(SkipHours::Hour5),
      6 => Ok(SkipHours::Hour6),
      7 => Ok(SkipHours::Hour7),
      8 => Ok(SkipHours::Hour8),
      9 => Ok(SkipHours::Hour9),
      10 => Ok(SkipHours::Hour10),
      11 => Ok(SkipHours::Hour11),
      12 => Ok(SkipHours::Hour12),
      13 => Ok(SkipHours::Hour13),
      14 => Ok(SkipHours::Hour14),
      15 => Ok(SkipHours::Hour15),
      16 => Ok(SkipHours::Hour16),
      17 => Ok(SkipHours::Hour17),
      18 => Ok(SkipHours::Hour18),
      19 => Ok(SkipHours::Hour19),
      20 => Ok(SkipHours::Hour20),
      21 => Ok(SkipHours::Hour21),
      22 => Ok(SkipHours::Hour22),
      23 => Ok(SkipHours::Hour23),
      found => Err(
        Error::InvalidData {
          for_type: "SkipHours".to_string(),
          expected: "0-23".to_string(),
          found: found.to_string(),
        }.into()
      )
    }
  }
}

impl Queryable<skip_hours::SqlType, Sqlite> for SkipHours {
    type Row = (i32,);

    fn build(row: Self::Row) -> deserialize::Result<Self> {
        Self::try_from(row.0)
    }
}

impl Selectable<Sqlite> for SkipHours {
    type SelectExpression = (skip_hours::id,);

    fn construct_selection() -> Self::SelectExpression {
        (skip_hours::id,)
    }
}

impl FromSql<Integer, Sqlite> for SkipHours {
  fn from_sql(value: <Sqlite as Backend>::RawValue<'_>) -> deserialize::Result<Self> {
    <i32 as FromSql<Integer, Sqlite>>::from_sql(value)
      .and_then(|i| SkipHours::try_from(i))
  }
}
