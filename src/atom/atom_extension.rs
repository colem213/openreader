use super::AtomLink;
use rss::extension::{Extension, ExtensionMap};

pub struct AtomExtension {
  pub id: Option<String>,
  pub links: Vec<AtomLink>,
  pub title: Option<String>,
  pub subtitle: Option<String>,
  pub summary: Option<String>,
  pub updated: Option<String>,
}

impl TryFrom<&ExtensionMap> for AtomExtension {
  type Error = ();

  fn try_from(value: &ExtensionMap) -> Result<Self, Self::Error> {
    let atom = match value.get("atom") {
      Some(atom) => atom,
      None => return Err(())
    };
    Ok(
      Self {
        id: atom.get("id")
          .and_then(|v| v.first().and_then(|v| Some(v.name.clone()))),
        links: atom.get("link")
          .map_or(Vec::new(), |v|
            v.iter().filter_map(|i| AtomLink::try_from(i).ok()).collect()
          ),
        title: atom.get("title")
          .and_then(|v| v.first().and_then(|v| Some(v.name.clone()))),
        subtitle: atom.get("subtitle")
          .and_then(|v| v.first().and_then(|v| Some(v.name.clone()))),
        summary: atom.get("summary")
          .and_then(|v| v.first().and_then(|v| Some(v.name.clone()))),
        updated: atom.get("updated")
          .and_then(|v|
            v.first().and_then(|v: &Extension| Some(v.name.clone()))
          ),
      }
    )
  }
}
