use super::sql_types::Uuid;

use crate::database::multi_connection_impl::MultiBackend;

use diesel::backend::Backend;
use diesel::deserialize::{self, FromSql};
use diesel::serialize::{self, IsNull, Output, ToSql};
use diesel::sqlite::{Sqlite, SqliteType, SqliteValue};
use diesel::sql_types::{Text, HasSqlType};

#[derive(Debug, Clone, Copy, Default, QueryId, FromSqlRow, AsExpression)]
#[diesel(sqlite_type(name = "Text"))]
#[diesel(sql_type = Uuid)]
pub struct UuidProxy {
  pub uuid: uuid::Uuid,
}

impl UuidProxy {
  fn new(uuid: uuid::Uuid) -> Self {
    UuidProxy { uuid }
  }
}

impl From<uuid::Uuid> for UuidProxy {
  fn from(uuid: uuid::Uuid) -> Self {
    UuidProxy::new(uuid)
  }
}

impl From<ulid::Ulid> for UuidProxy {
  fn from(ulid: ulid::Ulid) -> Self {
    UuidProxy::new(ulid.into())
  }
}

impl TryFrom<String> for UuidProxy {
  type Error = Box<dyn std::error::Error + Send + Sync>;

  fn try_from(value: String) -> Result<Self, Self::Error> {
    match uuid::Uuid::try_from(value.as_str()) {
      Ok(uuid) => Ok(UuidProxy::new(uuid)),
      Err(err) => Err(Box::new(err))
    }
  }
}

impl HasSqlType<Uuid> for MultiBackend {
  fn metadata(lookup: &mut Self::MetadataLookup) -> Self::TypeMetadata {
    MultiBackend::lookup_sql_type::<Uuid>(lookup)
  }
}

impl FromSql<Uuid, MultiBackend> for UuidProxy {
 fn from_sql(bytes: <MultiBackend as Backend>::RawValue<'_>) -> deserialize::Result<Self> {
    bytes.from_sql::<UuidProxy, Uuid>()
  }
}

impl ToSql<Uuid, MultiBackend> for UuidProxy {
  fn to_sql<'b>(
    &'b self,
    out: &mut serialize::Output<'b, '_, MultiBackend>,
  ) -> serialize::Result {
    out.set_value((Uuid, self));
    Ok(IsNull::No)
  }
}

impl HasSqlType<UuidProxy> for Sqlite {
  fn metadata(_lookup: &mut ()) -> SqliteType {
    SqliteType::Text
  }
}

impl FromSql<Uuid, Sqlite> for UuidProxy {
  fn from_sql(bytes: SqliteValue<'_, '_, '_>) -> deserialize::Result<Self> {
    <String as FromSql<Text, Sqlite>>::from_sql(bytes).and_then(|v|
      UuidProxy::try_from(v)
    )
  }
}

impl ToSql<Uuid, Sqlite> for UuidProxy {
  fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Sqlite>) -> serialize::Result {
    out.set_value(self.uuid.hyphenated().to_string());
    Ok(IsNull::No)
  }
}
