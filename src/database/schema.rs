// @generated automatically by Diesel CLI.

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feed_authors_atom_persons (feed_id, atom_person_id) {
        feed_id -> Uuid,
        atom_person_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feed_contributors_atom_persons (feed_id, atom_person_id) {
        feed_id -> Uuid,
        atom_person_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feed_rights_atom_texts (feed_id) {
        feed_id -> Uuid,
        atom_text_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feed_subtitles_atom_texts (feed_id) {
        feed_id -> Uuid,
        atom_text_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feed_titles_atom_texts (feed_id) {
        feed_id -> Uuid,
        atom_text_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feeds (id) {
        id -> Uuid,
        last_updated_at -> TimestamptzSqlite,
        generator_name -> Nullable<Text>,
        generator_uri -> Nullable<Text>,
        generator_version -> Nullable<Text>,
        language_id -> Nullable<Text>,
        base -> Nullable<Text>,
        icon -> Nullable<Text>,
        logo -> Nullable<Text>,
        created_at -> TimestamptzSqlite,
        updated_at -> TimestamptzSqlite,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feeds_atom_links (feed_id, atom_link_id) {
        feed_id -> Uuid,
        atom_link_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_feeds_atom_texts (feed_id, atom_text_id) {
        feed_id -> Uuid,
        atom_text_id -> Uuid,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_links (id) {
        id -> Uuid,
        href -> Text,
        rel -> Nullable<Text>,
        mime_type -> Nullable<Text>,
        title -> Nullable<Text>,
        length -> Nullable<Integer>,
        language_id -> Nullable<Text>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_persons (id) {
        id -> Uuid,
        name -> Text,
        email -> Nullable<Text>,
        uri -> Nullable<Text>,
        created_at -> TimestamptzSqlite,
        updated_at -> TimestamptzSqlite,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_text_kinds (id) {
        id -> Text,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    atom_texts (id) {
        id -> Uuid,
        atom_text_kind_id -> Text,
        text -> Text,
        base -> Nullable<Text>,
        language_id -> Nullable<Text>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    channels (id) {
        id -> Uuid,
        title -> Text,
        link -> Text,
        description -> Text,
        language -> Nullable<Text>,
        copyright -> Nullable<Text>,
        managing_editor -> Nullable<Text>,
        webmaster -> Nullable<Text>,
        published_at -> Nullable<TimestamptzSqlite>,
        last_build_at -> Nullable<TimestamptzSqlite>,
        generator -> Nullable<Text>,
        docs -> Nullable<Text>,
        rating -> Nullable<Text>,
        ttl -> Nullable<Integer>,
        image -> Nullable<Text>,
        created_at -> TimestamptzSqlite,
        updated_at -> TimestamptzSqlite,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    channels_skip_days (feed_id, skip_days_id) {
        feed_id -> Uuid,
        skip_days_id -> Integer,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    channels_skip_hours (feed_id, skip_hours_id) {
        feed_id -> Uuid,
        skip_hours_id -> Integer,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    channels_syndication_update_periods (feed_id, syndication_update_period_id) {
        feed_id -> Uuid,
        syndication_update_period_id -> Integer,
        frequency -> Integer,
        base -> TimestamptzSqlite,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    feed_kinds (id) {
        id -> Integer,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    feeds (id) {
        id -> Uuid,
        guid -> Text,
        link -> Text,
        kind -> Integer,
        created_at -> Text,
        updated_at -> Text,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    items (id) {
        id -> Integer,
        feed_id -> Uuid,
        title -> Text,
        description -> Nullable<Text>,
        created_at -> TimestamptzSqlite,
        updated_at -> TimestamptzSqlite,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    itunes_feed_extensions (id) {
        id -> Uuid,
        subtitle -> Nullable<Text>,
        kind -> Nullable<Text>,
        author -> Nullable<Text>,
        owner_name -> Nullable<Text>,
        owner_email -> Nullable<Text>,
        summary -> Nullable<Text>,
        explicit -> Nullable<Bool>,
        complete -> Bool,
        block -> Bool,
        image -> Nullable<Text>,
        new_feed_url -> Nullable<Text>,
        created_at -> TimestamptzSqlite,
        updated_at -> TimestamptzSqlite,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    languages (id) {
        id -> Text,
        name -> Text,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    skip_days (id) {
        id -> Integer,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    skip_hours (id) {
        id -> Integer,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use crate::database::sql_types::Uuid;

    syndication_update_periods (id) {
        id -> Integer,
        period -> Text,
        format -> Text,
    }
}

diesel::joinable!(atom_feed_authors_atom_persons -> atom_persons (atom_person_id));
diesel::joinable!(atom_feed_authors_atom_persons -> feeds (feed_id));
diesel::joinable!(atom_feed_contributors_atom_persons -> atom_persons (atom_person_id));
diesel::joinable!(atom_feed_contributors_atom_persons -> feeds (feed_id));
diesel::joinable!(atom_feed_rights_atom_texts -> atom_texts (atom_text_id));
diesel::joinable!(atom_feed_rights_atom_texts -> feeds (feed_id));
diesel::joinable!(atom_feed_subtitles_atom_texts -> atom_texts (atom_text_id));
diesel::joinable!(atom_feed_subtitles_atom_texts -> feeds (feed_id));
diesel::joinable!(atom_feed_titles_atom_texts -> atom_texts (atom_text_id));
diesel::joinable!(atom_feed_titles_atom_texts -> feeds (feed_id));
diesel::joinable!(atom_feeds -> feeds (id));
diesel::joinable!(atom_feeds -> languages (language_id));
diesel::joinable!(atom_feeds_atom_links -> atom_links (atom_link_id));
diesel::joinable!(atom_feeds_atom_links -> feeds (feed_id));
diesel::joinable!(atom_feeds_atom_texts -> atom_texts (atom_text_id));
diesel::joinable!(atom_feeds_atom_texts -> feeds (feed_id));
diesel::joinable!(atom_links -> languages (language_id));
diesel::joinable!(atom_texts -> atom_text_kinds (atom_text_kind_id));
diesel::joinable!(atom_texts -> languages (language_id));
diesel::joinable!(channels -> feeds (id));
diesel::joinable!(channels_skip_days -> feeds (feed_id));
diesel::joinable!(channels_skip_days -> skip_days (skip_days_id));
diesel::joinable!(channels_skip_hours -> feeds (feed_id));
diesel::joinable!(channels_skip_hours -> skip_hours (skip_hours_id));
diesel::joinable!(channels_syndication_update_periods -> feeds (feed_id));
diesel::joinable!(channels_syndication_update_periods -> syndication_update_periods (syndication_update_period_id));
diesel::joinable!(feeds -> feed_kinds (kind));
diesel::joinable!(items -> feeds (feed_id));
diesel::joinable!(itunes_feed_extensions -> feeds (id));

diesel::allow_tables_to_appear_in_same_query!(
    atom_feed_authors_atom_persons,
    atom_feed_contributors_atom_persons,
    atom_feed_rights_atom_texts,
    atom_feed_subtitles_atom_texts,
    atom_feed_titles_atom_texts,
    atom_feeds,
    atom_feeds_atom_links,
    atom_feeds_atom_texts,
    atom_links,
    atom_persons,
    atom_text_kinds,
    atom_texts,
    channels,
    channels_skip_days,
    channels_skip_hours,
    channels_syndication_update_periods,
    feed_kinds,
    feeds,
    items,
    itunes_feed_extensions,
    languages,
    skip_days,
    skip_hours,
    syndication_update_periods,
);
