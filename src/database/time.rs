use super::multi_connection_impl::MultiBackend;

use diesel::backend::Backend;
use diesel::deserialize::{self, FromSql};
use diesel::serialize::{self, IsNull, ToSql};
use diesel::sql_types::{HasSqlType, TimestamptzSqlite};
use time::OffsetDateTime;

impl HasSqlType<TimestamptzSqlite> for MultiBackend {
  fn metadata(lookup: &mut Self::MetadataLookup) -> Self::TypeMetadata {
    MultiBackend::lookup_sql_type::<TimestamptzSqlite>(lookup)
  }
}

impl FromSql<TimestamptzSqlite, MultiBackend> for OffsetDateTime {
 fn from_sql(bytes: <MultiBackend as Backend>::RawValue<'_>) -> deserialize::Result<Self> {
    bytes.from_sql::<OffsetDateTime, TimestamptzSqlite>()
  }
}

impl ToSql<TimestamptzSqlite, MultiBackend> for OffsetDateTime {
  fn to_sql<'b>(
    &'b self,
    out: &mut serialize::Output<'b, '_, MultiBackend>,
  ) -> serialize::Result {
    out.set_value((TimestamptzSqlite, self));
    Ok(IsNull::No)
  }
}
