-- Your SQL goes here
CREATE TABLE atom_feed_authors_atom_persons (
  feed_id TEXT NOT NULL,
  atom_person_id TEXT NOT NULL,
  PRIMARY KEY(feed_id, atom_person_id),
  FOREIGN KEY(feed_id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY(atom_person_id) REFERENCES atom_persons(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
)
