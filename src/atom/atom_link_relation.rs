#[derive(Debug, PartialEq, Eq)]
pub enum AtomLinkRelation {
  Alternate,
  Related,
  This,
  Enclosure,
  Via,
}

impl TryFrom<&String> for AtomLinkRelation {
  type Error = ();

  fn try_from(value: &String) -> Result<Self, Self::Error> {
    match value.to_lowercase().as_str() {
      "alternate" => Ok(AtomLinkRelation::Alternate),
      "related" => Ok(AtomLinkRelation::Related),
      "self" => Ok(AtomLinkRelation::This),
      "enclosure" => Ok(AtomLinkRelation::Enclosure),
      "via" => Ok(AtomLinkRelation::Via),
      _ => Err(()),
    }
  }
}
