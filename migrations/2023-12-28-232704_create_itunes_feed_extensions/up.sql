-- Your SQL goes here
CREATE TABLE itunes_feed_extensions (
  id TEXT PRIMARY KEY NOT NULL,
  subtitle TEXT,
  kind TEXT,
  author TEXT,
  owner_name TEXT,
  owner_email TEXT,
  summary TEXT,
  explicit INTEGER,
  complete INTEGER NOT NULL DEFAULT 0,
  block INTEGER NOT NULL DEFAULT 0,
  image TEXT,
  new_feed_url TEXT,
  created_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY(id) REFERENCES feeds(id) ON DELETE CASCADE
)
