CREATE TABLE channels_skip_hours (
  feed_id TEXT NOT NULL,
  skip_hours_id INTEGER NOT NULL,
  PRIMARY KEY(feed_id, skip_hours_id),
  FOREIGN KEY(feed_id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY(skip_hours_id) REFERENCES skip_hours(id)
)
