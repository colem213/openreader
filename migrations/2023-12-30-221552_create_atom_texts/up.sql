-- Your SQL goes here
CREATE TABLE atom_texts (
  id TEXT PRIMARY KEY NOT NULL,
  atom_text_kind_id TEXT NOT NULL,
  text TEXT NOT NULL,
  base TEXT,
  language_id TEXT,
  FOREIGN KEY (language_id) REFERENCES languages(id),
  FOREIGN KEY (atom_text_kind_id) REFERENCES atom_text_kinds(id)
)
