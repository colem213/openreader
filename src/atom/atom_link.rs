use super::AtomLinkRelation;

use rss::extension::Extension;

pub struct AtomLink {
  pub href: String,
  pub rel: Option<AtomLinkRelation>,
  pub mime_type: Option<String>,
  pub title: Option<String>,
  pub length: Option<String>,
}

impl TryFrom<&Extension> for AtomLink {
  type Error = ();

  fn try_from(value: &Extension) -> Result<Self, Self::Error> {
    let link = match value.attrs.get("href") {
      Some(link) => link,
      None => return Err(())
    };
    Ok(
      Self {
        href: link.clone(),
        rel: value.attrs.get("rel")
          .and_then(|v| AtomLinkRelation::try_from(v).ok()),
        mime_type: value.attrs.get("type").cloned(),
        title: value.attrs.get("title").cloned(),
        length: value.attrs.get("length").cloned(),
      }
    )
  }
}
