-- Your SQL goes here
CREATE TABLE atom_links (
  id TEXT PRIMARY KEY NOT NULL,
  href TEXT NOT NULL,
  rel TEXT,
  mime_type TEXT,
  title TEXT,
  length INTEGER,
  language_id TEXT,
  FOREIGN KEY (language_id) REFERENCES languages(id)
)
