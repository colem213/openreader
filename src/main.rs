use std::env;
use std::fs::File;
use std::io::BufReader;

use dotenvy::dotenv;
use rss::Channel;
use openreader::database::Db;

use tracing::debug;
use tracing_appender::rolling::{RollingFileAppender, Rotation};
use tracing_subscriber::{EnvFilter, fmt::writer::MakeWriterExt};

fn main() {
  dotenv().ok();

  let (non_blocking_stdout, _guard) = tracing_appender::non_blocking(
    std::io::stdout()
  );
  let file_appender = RollingFileAppender::new(
    Rotation::NEVER, ".", "openreader.log"
  );
  tracing_subscriber::fmt()
    .with_env_filter(EnvFilter::from_default_env())
    .with_writer(non_blocking_stdout.and(file_appender))
    .init();

  let filename = env::args().nth(1);
  debug!(?filename);

  let db = Db::default();
  filename.and_then(|f| {
    let file = File::open(f).unwrap();
    let channel = Channel::read_from(BufReader::new(file)).unwrap();
    db.create_channel(channel);
    Some(())
  });
}
