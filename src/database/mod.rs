use std::env;
use std::ops::DerefMut;

mod schema;
mod sql_types;

mod time;

mod uuid_proxy;
use uuid_proxy::UuidProxy;

mod models;
use models::Channel;

use diesel::connection::SimpleConnection;
use diesel::prelude::*;
use diesel::r2d2::{ConnectionManager, CustomizeConnection, Error as R2D2Error, Pool};
use diesel_tracing::sqlite::InstrumentedSqliteConnection;

use crate::database::models::{Feed, FeedKind};

#[derive(diesel::MultiConnection)]
pub enum AnyConnection {
  Sqlite(InstrumentedSqliteConnection),
}

#[derive(Debug)]
pub struct Db {
  pool: Pool<ConnectionManager<AnyConnection>>
}

impl Db {
  pub fn default() -> Self {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set");
    let manager = ConnectionManager::<AnyConnection>::new(database_url);
    let pool = Pool::builder()
      .connection_customizer(Box::new(ConnectionOptions))
      .build(manager)
      .expect("Could not build connection pool");
    Self { pool }
  }

  fn channel(&self) -> QueryResult<Vec<Channel>> {
    use schema::channels;

    let mut binding = self.pool.clone().get().unwrap();
    let conn = binding.deref_mut();
    return channels::table.select(Channel::as_select()).load::<Channel>(conn);
  }

  fn feed(&self) {
    use schema::feeds::dsl::*;

    let mut binding = self.pool.clone().get().unwrap();
    let conn = binding.deref_mut();
    diesel::insert_into(feeds)
      .values(
        &Feed::new(
          ulid::Ulid::new().into(),
          "guid".into(),
          "https://www.link.com".into(),
          FeedKind::Rss,
        )
      )
      .get_result(conn);
  }
}

#[derive(Copy, Clone, Debug)]
pub struct ConnectionOptions;

impl CustomizeConnection<AnyConnection, R2D2Error> for ConnectionOptions {
  fn on_acquire(&self, conn: &mut AnyConnection) -> Result<(), R2D2Error> {
    match conn {
      AnyConnection::Sqlite(ref mut conn) => {
        (|| {
          conn.batch_execute(
            "PRAGMA journal_mode = WAL; PRAGMA synchronous = NORMAL; \
            PRAGMA foreign_keys = ON; PRAGMA busy_timeout = 30000; \
            PRAGMA temp_store = memory;"
            )?;
          Ok(())
        })()
        .map_err(diesel::r2d2::Error::QueryError)
      }
    }
  }
}
