CREATE TABLE channels_skip_days (
  feed_id TEXT NOT NULL,
  skip_days_id INTEGER NOT NULL,
  PRIMARY KEY(feed_id, skip_days_id),
  FOREIGN KEY(feed_id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY(skip_days_id) REFERENCES skip_days(id)
)
