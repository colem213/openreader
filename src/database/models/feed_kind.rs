use crate::database::schema::feed_kinds;
use crate::error::Error;

use diesel::{Queryable, Selectable};
use diesel::deserialize::{self, FromSql};
use diesel::sqlite::{Sqlite, SqliteValue};
use diesel::sql_types::Integer;

#[derive(Debug, Clone, Copy)]
pub enum FeedKind {
  Rss = 0,
  Atom = 1,
}

impl TryFrom<i32> for FeedKind {
  type Error = Box<dyn std::error::Error + Send + Sync>;

  fn try_from(value: i32) -> Result<Self, Self::Error> {
    match value {
      0 => Ok(FeedKind::Rss),
      1 => Ok(FeedKind::Atom),
      found => Err(
        Error::InvalidData {
          for_type: "FeedKind".to_string(),
          expected: "0,1".to_string(),
          found: found.to_string(),
        }.into()
      )
    }
  }
}

impl Into<i32> for FeedKind {
  fn into(self) -> i32 {
    match self {
      FeedKind::Rss => 0,
      FeedKind::Atom => 1,
    }
  }
}

impl Queryable<feed_kinds::SqlType, Sqlite> for FeedKind {
  type Row = (i32,);

  fn build(row: Self::Row) -> deserialize::Result<Self> {
      Self::try_from(row.0)
  }
}

impl Selectable<Sqlite> for FeedKind {
  type SelectExpression = (feed_kinds::id,);

  fn construct_selection() -> Self::SelectExpression {
      (feed_kinds::id,)
  }
}

impl FromSql<Integer, Sqlite> for FeedKind {
  fn from_sql(bytes: SqliteValue<'_, '_, '_>) -> deserialize::Result<Self> {
    <i32 as FromSql<Integer, Sqlite>>::from_sql(bytes)
      .and_then(|i| FeedKind::try_from(i))
  }
}
