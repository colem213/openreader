use crate::database::schema::skip_days;
use crate::error::Error;

use diesel::{Queryable, Selectable};
use diesel::backend::Backend;
use diesel::deserialize::{self, FromSql};
use diesel::sqlite::Sqlite;
use diesel::sql_types::Integer;

#[derive(Debug, Clone, Copy)]
pub enum SkipDays {
  Monday = 0,
  Tuesday = 1,
  Wednesday = 2,
  Thursday = 3,
  Friday = 4,
  Saturday = 5,
  Sunday = 6
}

impl TryFrom<i32> for SkipDays {
  type Error = Box<dyn std::error::Error + Send + Sync>;

  fn try_from(value: i32) -> Result<Self, Self::Error> {
    match value {
      0 => Ok(SkipDays::Monday),
      1 => Ok(SkipDays::Tuesday),
      2 => Ok(SkipDays::Wednesday),
      3 => Ok(SkipDays::Thursday),
      4 => Ok(SkipDays::Friday),
      5 => Ok(SkipDays::Saturday),
      6 => Ok(SkipDays::Sunday),
      found => Err(
        Error::InvalidData {
          for_type: "SkipDays".to_string(),
          expected: "0-6".to_string(),
          found: found.to_string(),
        }.into()
      )
    }
  }
}

impl Queryable<skip_days::SqlType, Sqlite> for SkipDays {
  type Row = (i32,);

  fn build(row: Self::Row) -> deserialize::Result<Self> {
      Self::try_from(row.0)
  }
}

impl Selectable<Sqlite> for SkipDays {
  type SelectExpression = (skip_days::id,);

  fn construct_selection() -> Self::SelectExpression {
      (skip_days::id,)
  }
}

impl FromSql<Integer, Sqlite> for SkipDays {
  fn from_sql(value: <Sqlite as Backend>::RawValue<'_>) -> deserialize::Result<Self> {
    <i32 as FromSql<Integer, Sqlite>>::from_sql(value)
      .and_then(|i| SkipDays::try_from(i))
  }
}
