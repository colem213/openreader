-- Your SQL goes here
CREATE TABLE atom_feeds (
  id TEXT PRIMARY KEY NOT NULL,
  last_updated_at TEXT NOT NULL,
  generator_name TEXT,
  generator_uri TEXT,
  generator_version TEXT,
  language_id TEXT,
  base TEXT,
  icon TEXT,
  logo TEXT,
  created_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY(language_id) REFERENCES languages(id)
  FOREIGN KEY(id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
)
