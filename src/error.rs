use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
  #[error("invalid data for {for_type:?} (expected {expected:?}, found {found:?})")]
  InvalidData {
    for_type: String,
    expected: String,
    found: String,
  },
}
