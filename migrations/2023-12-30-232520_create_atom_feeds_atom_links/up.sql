-- Your SQL goes here
CREATE TABLE atom_feeds_atom_links (
  feed_id TEXT NOT NULL,
  atom_link_id TEXT NOT NULL,
  PRIMARY KEY(feed_id, atom_link_id),
  FOREIGN KEY(feed_id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY(atom_link_id) REFERENCES atom_links(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
)
