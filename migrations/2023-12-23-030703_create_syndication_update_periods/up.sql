-- Your SQL goes here
CREATE TABLE syndication_update_periods(
  id INTEGER PRIMARY KEY NOT NULL,
  period TEXT NOT NULL,
  format TEXT NOT NULL
)
