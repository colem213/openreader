-- Your SQL goes here
CREATE TABLE channels (
  id TEXT PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  link TEXT UNIQUE NOT NULL,
  description TEXT NOT NULL,
  language TEXT,
  copyright TEXT,
  managing_editor TEXT,
  webmaster TEXT,
  published_at TEXT,
  last_build_at TEXT,
  generator TEXT,
  docs TEXT,
  rating TEXT,
  ttl INTEGER,
  image TEXT,
  created_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TEXT NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY(id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
)
