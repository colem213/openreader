use rss::extension::Extension;

pub struct PodcastTranscript {
  pub url: String,
  pub mime_type: String,
  pub rel: Option<String>,
  pub language: Option<String>,
}

impl TryFrom<&Extension> for PodcastTranscript {
  type Error = ();

  fn try_from(value: &Extension) -> Result<Self, Self::Error> {
    let url = match value.attrs.get("url") {
      Some(url) => url,
      None => return Err(())
    };
    let mime_type = match value.attrs.get("type") {
      Some(mime_type) => mime_type,
      None => return Err(())
    };
    Ok(
      Self {
        url: url.clone(),
        mime_type: mime_type.clone(),
        rel: value.attrs.get("rel").and_then(|v| Some(v.clone())),
        language: value.attrs.get("language").and_then(|v| Some(v.clone())),
      }
    )
  }
}
