-- Your SQL goes here
CREATE TABLE atom_feeds_atom_texts (
  feed_id TEXT NOT NULL,
  atom_text_id TEXT NOT NULL,
  PRIMARY KEY(feed_id, atom_text_id),
  FOREIGN KEY(feed_id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY(atom_text_id) REFERENCES atom_texts(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
)
