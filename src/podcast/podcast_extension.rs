use super::{PodcastFunding, PodcastTranscript};

use rss::extension::ExtensionMap;

pub struct PodcastExtension {
  pub guid: Option<String>,
  pub funding: Option<PodcastFunding>,
  pub transcripts: Vec<PodcastTranscript>,
}

impl TryFrom<&ExtensionMap> for PodcastExtension {
  type Error = ();

  fn try_from(value: &ExtensionMap) -> Result<Self, Self::Error> {
    let podcast = match value.get("podcast") {
      Some(podcast) => podcast,
      None => return Err(())
    };
    Ok(
      PodcastExtension {
        guid: podcast.get("guid").and_then(|v|
          v.first().and_then(|v| v.value.clone())
        ),
        funding: podcast.get("funding")
          .and_then(|v|v.first())
          .and_then(|v| PodcastFunding::try_from(v).ok()),
        transcripts: podcast.get("transcript").map_or(Vec::new(), |v|
          v.iter().filter_map(|i|
            PodcastTranscript::try_from(i).ok()
          ).collect()
        ),
      }
    )
  }
}
