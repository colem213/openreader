-- Your SQL goes here
CREATE TRIGGER channels_updated_at
AFTER UPDATE OF
  title, link, description, language,
  copyright, managing_editor, webmaster,
  published_at, last_build_at, generator, docs,
  rating, ttl, image
ON channels WHEN
  NEW.title IS NOT OLD.title OR NEW.link IS NOT OLD.link OR NEW.description IS NOT OLD.description OR
  NEW.language IS NOT OLD.language OR NEW.copyright IS NOT OLD.copyright OR NEW.managing_editor IS NOT OLD.managing_editor OR NEW.webmaster IS NOT OLD.webmaster OR
  NEW.published_at IS NOT OLD.published_at OR NEW.last_build_at IS NOT OLD.last_build_at OR NEW.generator IS NOT OLD.generator OR NEW.docs IS NOT OLD.docs OR
  NEW.rating IS NOT OLD.rating OR NEW.ttl IS NOT OLD.ttl OR NEW.image IS NOT OLD.image
BEGIN
  UPDATE channels SET updated_at = CURRENT_TIMESTAMP WHERE id = NEW.id;
END
