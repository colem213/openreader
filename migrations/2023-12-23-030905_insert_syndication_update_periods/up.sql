-- Your SQL goes here
INSERT INTO syndication_update_periods (id, period, format) VALUES (1, "hourly", "%H"), (2, "daily", "%j"), (3, "weekly", "%W"), (4, "monthly", "%m"), (5, "yearly", "%Y")
