# Open Reader

RSS feed aggregator

## Resources
[Polling Policy](https://stackoverflow.com/a/939709)  
[Reddit RSS Wiki](https://www.reddit.com/r/AskReddit/new/.rss)

## RSS Feed Examples
[http://rss.cnn.com/rss/cnn_topstories.rss](http://rss.cnn.com/rss/cnn_topstories.rss)
[https://www.reddit.com/r/AskReddit/new/.rss](https://www.reddit.com/r/AskReddit/new/.rss)
