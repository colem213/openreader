use crate::database::{schema::feeds, UuidProxy};

use super::FeedKind;

#[derive(Debug, Queryable, Selectable, Insertable, AsChangeset)]
#[diesel(table_name = feeds)]
pub struct Feed {
  pub id: UuidProxy,
  pub guid: String,
  pub link: String,
  pub kind: i32,
}

impl Feed {
  pub fn new(
    id: UuidProxy, guid: String, link: String, kind: FeedKind,
  ) -> Self {
    Feed {
      id,
      guid,
      link,
      kind: kind.into(),
    }
  }
}
