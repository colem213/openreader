use std::error::Error as StdError;

use crate::database::schema::channels_syndication_update_periods;
use crate::database::UuidProxy;

use rss::extension::syndication::{SyndicationExtension, UpdatePeriod};
use time::{OffsetDateTime, format_description::well_known::Iso8601};

#[derive(Debug, Insertable)]
#[diesel(table_name = channels_syndication_update_periods)]
pub struct ChannelSyndicationUpdatePeriod {
  pub feed_id: UuidProxy,
  pub syndication_update_period_id: i32,
  pub frequency: i32,
  pub base: OffsetDateTime,
}

impl ChannelSyndicationUpdatePeriod {
  pub fn new(
    feed_id: UuidProxy,
    syndication_ext: SyndicationExtension,
  ) -> Result<Self, Box<dyn StdError>> {
    let base = OffsetDateTime::parse(syndication_ext.base(), &Iso8601::DATE_TIME)?;
    let frequency = i32::try_from(syndication_ext.frequency)?;
    let syndication_update_period_id = match syndication_ext.period {
        UpdatePeriod::Hourly => 1,
        UpdatePeriod::Daily => 2,
        UpdatePeriod::Weekly => 3,
        UpdatePeriod::Monthly => 4,
        UpdatePeriod::Yearly => 5,
    };
    Ok(
      Self {
        feed_id,
        syndication_update_period_id,
        frequency,
        base,
      }
    )
  }
}
