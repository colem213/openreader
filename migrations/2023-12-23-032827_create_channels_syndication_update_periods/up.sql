-- Your SQL goes here
CREATE TABLE channels_syndication_update_periods (
  feed_id TEXT UNIQUE NOT NULL,
  syndication_update_period_id INTEGER NOT NULL,
  frequency INTEGER NOT NULL,
  base TEXT NOT NULL,
  PRIMARY KEY(feed_id, syndication_update_period_id),
  FOREIGN KEY(feed_id) REFERENCES feeds(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED,
  FOREIGN KEY(syndication_update_period_id) REFERENCES syndication_update_periods(id)
)
