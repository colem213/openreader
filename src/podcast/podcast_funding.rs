use rss::extension::Extension;

pub struct PodcastFunding {
  pub url: String,
  pub text: String,
}

impl TryFrom<&Extension> for PodcastFunding {
  type Error = ();

  fn try_from(value: &Extension) -> Result<Self, Self::Error> {
    let url = match value.attrs.get("url") {
      Some(url) => url,
      None => return Err(())
    };
    let text = match value.value.as_ref() {
      Some(text) => text,
      None => return Err(())
    };
    Ok(
      Self {
        url: url.clone(),
        text: text.clone(),
      }
    )
  }
}
